license = <<-EOF
Copyright (c) 2015, Bai Tian Corp.
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
EOF

Pod::Spec.new do |s|

# 模块名
  s.name         = "YZCoreDataTable"
# 模块版本号
  s.version      = "0.0.1"
# 简要描述
  s.summary      = "YZCoreDataTable 封装CoreDate跟Table的行为"
# 详细描述
  s.description  = <<-DESC
                    YZCoreDataTable 封装CoreDate跟Table的行为,高度模块化相关操作
                   DESC
# 模块主页
  s.homepage     = "https://bitbucket.org/YauzZ/coredata-component"


  s.license      = { :type => "BSD", :text => license }

# 作者信息
  s.author             = { "YauzZ" => "yauzz.scu@gmail.com" }

# 最低支持的ios系统版本
  s.platform     = :ios, "6.0"

# git代码库
s.source       = { :git => "https://YauzZ@bitbucket.org/YauzZ/coredata-component.git" , :tag => "v0.0.1"}

# 模块源代码目录
  s.source_files  = "Classes", "Classes/**/*.{h,m}"
# 模块源代码排除目录
  s.exclude_files = "Classes/Exclude"


# 是否需要arc支持
  s.requires_arc = true

# 模块依赖

# 资源文件路径
#s.ios.resources = ['Resources/**/*.{png,storyboard,xib}','Classes/**/*.{storyboard,xib}']

# 预编译头文件
s.prefix_header_contents = '#import <UIKit/UIKit.h>', '#import <Foundation/Foundation.h>','#import <CoreData/CoreData.h>'

end
