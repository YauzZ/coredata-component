//
//  People.h
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSManagedObject+CoreDataManager.h"

@interface People : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSNumber * isMale;
@property (nonatomic, retain) NSNumber * age;

@end
