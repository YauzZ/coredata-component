//
//  CoreDataModel.h
//  CoreDataTableViewDemo
//
//  Created by yauzz on 15/1/30.
//  Copyright (c) 2015年 YauzZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol CoreDataModelInterface <NSObject>

@required
+ (NSFetchedResultsController *)fetchedResultsController;

@optional


@end
