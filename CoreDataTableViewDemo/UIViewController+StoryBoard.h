//
//  UIViewController+StoryBoard.h
//  ElecFans
//
//  Created by yauzz on 15/1/24.
//  Copyright (c) 2015年 elecfans.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (StoryBoard)

+ (instancetype)instantiate;

+ (instancetype)instantiateWithStoryBoardName:(NSString *)name;

@end
