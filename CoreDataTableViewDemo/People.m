//
//  People.m
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import "People.h"
#import "NSString+random.h"

@implementation People

@dynamic name;
@dynamic birthday;
@dynamic isMale;
@dynamic age;

+ (NSFetchedResultsController *)fetchedResultsController
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"People"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"birthday" ascending:YES];
    [fetchRequest setSortDescriptors:@[
                                       [NSSortDescriptor sortDescriptorWithKey:@"age" ascending:YES],
                                       sortDescriptor]];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[People sharedManager] mainObjectContext] sectionNameKeyPath:nil cacheName:@"People"];
    
    return fetchedResultsController;
}

- (void)modelDidLoad
{
    self.name = [NSString randomString];
    self.isMale = rand()%2 == 0 ? @YES : @NO;
    self.age = [NSNumber numberWithInt:rand()%100];
    self.birthday = [NSDate dateWithTimeIntervalSince1970:(double)random()];
}

@end
