//
//  main.m
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
