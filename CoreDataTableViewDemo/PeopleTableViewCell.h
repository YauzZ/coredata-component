//
//  PeopleTableViewCell.h
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+Identifier.h"

@interface PeopleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;

@end
