//
//  NSString+random.h
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (random)

+ (NSString *)randomString;

@end
