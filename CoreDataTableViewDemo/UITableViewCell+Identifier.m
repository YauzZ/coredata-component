//
//  UITableViewCell+Identifier.m
//  ElecFans
//
//  Created by yauzz on 15/1/24.
//  Copyright (c) 2015年 elecfans.com. All rights reserved.
//

#import "UITableViewCell+Identifier.h"

@implementation UITableViewCell (Identifier)

+ (NSString *)identifier
{
    return NSStringFromClass(self);
}

@end
