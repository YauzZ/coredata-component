//
//  RootTableViewController.m
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import "RootTableViewController.h"
#import "CoreDataTableView.h"
#import "People.h"
#import "PeopleTableViewCell.h"

@interface RootTableViewController ()

@property (weak, nonatomic) IBOutlet CoreDataTableView *coreDataTableView;

@end

@implementation RootTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_coreDataTableView SetupWithCellIdentifier:[PeopleTableViewCell identifier] configureCellBlock:^(PeopleTableViewCell *cell, People *people) {
        cell.nameLabel.text = people.name;
        cell.ageLabel.text = [people.age description];
        cell.maleLabel.text = [people.isMale boolValue] ? @"男" : @"女";
        
        // 日期处理示例
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-DD"];
        NSString *currentDateStr = [dateFormatter stringFromDate:people.birthday];
        cell.birthdayLabel.text = currentDateStr;
    }];
    _coreDataTableView.fetchedResultsController = [People fetchedResultsController];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tapClearAll:(id)sender {
    [[People sharedManager] removeAllManagedObjectsWithFetchRequestTemplateName:@"FT_People_All"];
}

- (IBAction)tapAdd:(id)sender
{
    [People aNewManagedObject];
    [[People sharedManager] save];
}
- (IBAction)tapEdit:(id)sender {
    [self.coreDataTableView setEditing:!_coreDataTableView.editing animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
