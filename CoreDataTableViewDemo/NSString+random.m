//
//  NSString+random.m
//  CoreDataTableViewDemo
//
//  Created by YauzZ on 17/5/14.
//  Copyright (c) 2014年 YauzZ. All rights reserved.
//

#import "NSString+random.h"

@implementation NSString (random)

+ (NSString *)randomString
{
    return [NSString stringWithFormat:@"%d",rand()%1000];
}

@end
