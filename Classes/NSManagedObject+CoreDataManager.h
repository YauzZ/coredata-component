//
//  NSManagedObject+CoreDataManager.h
//  CoreDataTableViewDemo
//
//  Created by yauzz on 15/1/30.
//  Copyright (c) 2015年 YauzZ. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CoreDataManager.h"

@interface NSManagedObject (CoreDataManager)

+ (NSManagedObject *)aNewManagedObject;
+ (CoreDataManager *)sharedManager;

+ (NSFetchedResultsController *)fetchedResultsController;

- (void)modelDidLoad;

@end
