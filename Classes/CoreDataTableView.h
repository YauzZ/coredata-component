//
//  CoreDataTableView.h
//  BoriLearning
//
//  Created by YauzZ on 16/5/14.
//  Copyright (c) 2014年 Jason Tse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CoreDataTableView : UITableView <UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic,copy) void (^configureCellBlock)(id objectCell, id object);
@property (nonatomic,copy) NSString *cellIdentifier;

- (void)SetupWithCellIdentifier:(NSString *)identifier configureCellBlock:(void(^)(id cell, id object))configureCellBlock;

- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

@end
