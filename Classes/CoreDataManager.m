//
//  CoreDataManager.m
//  BoriLearning
//
//  Created by YauzZ on 14/5/14.
//  Copyright (c) 2014年 Jason Tse. All rights reserved.
//

#import "CoreDataManager.h"
#import "UpdateManager.h"
#import <CoreData/CoreData.h>

@interface CoreDataManager ()

@property (nonatomic, strong) NSString *modelName;

@end

static NSMutableDictionary *__sharedInstanceDict = nil;

static NSLock *__locker = nil;

@implementation CoreDataManager

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize mainObjectContext = _mainObjectContext;
@synthesize objectModel = _objectModel;

+ (id)sharedInstance
{
    return [self sharedInstance:defaultModelName];
}

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstanceDict = [NSMutableDictionary dictionary];
        __locker = [NSLock new];
    });
}

+ (instancetype)sharedInstance:(NSString *)modelName
{
    
    static CoreDataManager *sharedInstance = nil;

    if (!modelName) {
        return sharedInstance;
    }

    sharedInstance = [__sharedInstanceDict objectForKey:modelName];
    
    if (!sharedInstance) {
        
        [__locker lock];
        sharedInstance = [[self alloc] init];
        [__sharedInstanceDict setObject:sharedInstance forKey:modelName];
        sharedInstance.modelName = modelName;

        [__locker unlock];
    }
    
    return sharedInstance;
}

- (NSString *)modelName
{
    if (!_modelName) {
        [NSException exceptionWithName:@"未定义数据库名" reason:@"未定义数据库名" userInfo:nil];
    }
    
    return _modelName;
}

#pragma mark - an existing managed objects for entity
- (id)existingManagedObjectsWithFetchRequestTemplateName:(NSString *)frName predicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = self.mainObjectContext;
    NSFetchRequest *fetchRequest = [[self.objectModel fetchRequestTemplateForName:frName] copy];
    if (predicate) [fetchRequest setPredicate:predicate];
    NSArray *objs = [context executeFetchRequest:fetchRequest error:nil];
    
    return ([objs count] > 0)?([objs objectAtIndex:0]):nil;
}

#pragma mark - CoreData Utils

- (id)aManagedObjectWithEntityName:(NSString *)name
{
    id obj = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:self.mainObjectContext];
    return obj;
}

- (void)removeAllManagedObjectsWithFetchRequestTemplateName:(NSString *)name
{
    [self removeAllManagedObjectsWithFetchRequestTemplateName:name predicate:nil];
}

- (void)removeAllManagedObjectsWithFetchRequestTemplateName:(NSString *)name predicate:(NSPredicate *)predicate
{
    NSArray *managedObjects = [self allManagedObjectsForEntityWithFetchRequestTemplateName:name sortDescriptor:nil];
    
    if (predicate) {
        managedObjects = [managedObjects filteredArrayUsingPredicate:predicate];
    }
    
    if ([managedObjects count] > 0) {
        for (id obj in managedObjects) {
            [self.mainObjectContext deleteObject:obj];
        }
        [self save];
    }
}

- (NSArray *)allManagedObjectsForEntityWithFetchRequestTemplateName:(NSString *)frName sortDescriptor:(NSSortDescriptor *)sortDescriptor
{
    return [self allManagedObjectsForEntityWithFetchRequestTemplateName:frName sortDescriptor:sortDescriptor predicate:nil];
}

- (NSArray *)allManagedObjectsForEntityWithFetchRequestTemplateName:(NSString *)frName sortDescriptor:(NSSortDescriptor *)sortDescriptor predicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = self.mainObjectContext;
    NSFetchRequest *fetchRequest = [[self.objectModel fetchRequestTemplateForName:frName] copy];
    
    [fetchRequest setPredicate:predicate];
    
    if (sortDescriptor) {
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
    }
    NSError *error;
    NSArray *managedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (!managedObjects) {
        NSLog(@"allManagedObjectsForEntityWithFetchRequestTemplateName, execute fetch request error: %@", error);
        return nil;
    }
    else {
        return managedObjects;
    }
}

#pragma mark - system configuration
- (NSManagedObjectModel *)objectModel
{
    if (_objectModel) {
        return _objectModel;
    }
    
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:self.modelName ofType:@"momd"];
    NSAssert1(modelPath, @"modelPath %@ is nil", modelPath);
    
    _objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
    
    return _objectModel;
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator {
	if (_persistentStoreCoordinator)
		return _persistentStoreCoordinator;
    
	// Get the paths to the SQLite file
	NSString *storePath = [[self sharedDocumentsPath] stringByAppendingPathComponent:[self.modelName stringByAppendingString:@".sqlite"]];
    NSAssert(storePath, @"storePath %@ is nil", storePath);
    
//   [UpdateManager updateDataBaseWithName:self.modelName storePath:storePath];
    
	NSURL *storeURL = [NSURL fileURLWithPath:storePath];
    self.isFirstRun = ![storeURL checkResourceIsReachableAndReturnError:NULL];
    
	// Define the Core Data version migration options
	NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES};
    
	// Attempt to load the persistent store
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.objectModel];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:options
                                                           error:&error]) {
		NSLog(@"Fatal error while creating persistent store: %@", error);
        //		abort();
	}
        
	return _persistentStoreCoordinator;
}

- (NSString*)sharedDocumentsPath {
	static NSString *SharedDocumentsPath = nil;
	if (SharedDocumentsPath)
		return SharedDocumentsPath;
    
	// Compose a path to the <Library>/Database directory
	NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	SharedDocumentsPath = [libraryPath stringByAppendingPathComponent:@"Database"];
    
	// Ensure the database directory exists
	NSFileManager *manager = [NSFileManager defaultManager];
	BOOL isDirectory;
	if (![manager fileExistsAtPath:SharedDocumentsPath isDirectory:&isDirectory] || !isDirectory) {
		NSError *error = nil;
		NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
		[manager createDirectoryAtPath:SharedDocumentsPath
		   withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
		if (error)
			NSLog(@"Error creating directory path: %@", [error localizedDescription]);
	}
    
	return SharedDocumentsPath;
}

- (NSManagedObjectContext*)mainObjectContext {
	if (_mainObjectContext)
		return _mainObjectContext;
    
	// Create the main context only on the main thread
	if (![NSThread isMainThread]) {
		[self performSelectorOnMainThread:@selector(mainObjectContext)
                               withObject:nil
                            waitUntilDone:YES];
		return _mainObjectContext;
	}
    
	_mainObjectContext = [[NSManagedObjectContext alloc] init];
	[_mainObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    
	return _mainObjectContext;
}

- (BOOL)save {
	if (![self.mainObjectContext hasChanges]) return YES;
    
	NSError *error = nil;
	if (![self.mainObjectContext save:&error]) {
		NSLog(@"Error while saving: %@\n%@", [error localizedDescription], [error userInfo]);
		return NO;
	}
    
	return YES;
}

- (void)deleteManagedObject:(id)object
{
    [self.mainObjectContext deleteObject:object];
    [self save];
}

- (void)dealloc {
    [self save];
}

- (void)rollback
{
    if ([self.mainObjectContext hasChanges]) {
        [self.mainObjectContext rollback];
    }
}

@end
