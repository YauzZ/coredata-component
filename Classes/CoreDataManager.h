//
//  CoreDataManager.h
//  BoriLearning
//
//  Created by YauzZ on 14/5/14.
//  Copyright (c) 2014年 Jason Tse. All rights reserved.
//

static NSString *defaultModelName = @"DefaultModelName";

@interface CoreDataManager : NSObject

@property (strong, readonly, nonatomic) NSManagedObjectModel *objectModel;
@property (strong, readonly, nonatomic) NSManagedObjectContext *mainObjectContext;
@property (strong, readonly, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (id)sharedInstance;
+ (id)sharedInstance:(NSString *)modelName;

@property (assign, nonatomic) BOOL isFirstRun;

- (id)aManagedObjectWithEntityName:(NSString *)name;
- (void)deleteManagedObject:(id)object;
- (BOOL)save;
- (void)rollback;

- (id)existingManagedObjectsWithFetchRequestTemplateName:(NSString *)frName predicate:(NSPredicate *)predicate;

- (void)removeAllManagedObjectsWithFetchRequestTemplateName:(NSString *)name;
- (void)removeAllManagedObjectsWithFetchRequestTemplateName:(NSString *)name predicate:(NSPredicate *)predicate;

- (NSArray *)allManagedObjectsForEntityWithFetchRequestTemplateName:(NSString *)frName sortDescriptor:(NSSortDescriptor *)sortDescriptor;
- (NSArray *)allManagedObjectsForEntityWithFetchRequestTemplateName:(NSString *)frName sortDescriptor:(NSSortDescriptor *)sortDescriptor predicate:(NSPredicate *)predicate;

@end
