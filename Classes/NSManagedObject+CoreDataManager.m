//
//  NSManagedObject+CoreDataManager.m
//  CoreDataTableViewDemo
//
//  Created by yauzz on 15/1/30.
//  Copyright (c) 2015年 YauzZ. All rights reserved.
//

#import "NSManagedObject+CoreDataManager.h"

@implementation NSManagedObject (CoreDataManager)

+ (NSManagedObject *)aNewManagedObject
{
    NSManagedObject *managedObject =  [[self sharedManager] aManagedObjectWithEntityName:NSStringFromClass(self)];
    [managedObject modelDidLoad];
    return managedObject;
}

+ (CoreDataManager *)sharedManager
{
    // 实例的参数即是 xcdatamodeld 文件名，默认跟 model 名一致。如果不同，在 model 内覆盖这个方法即可。
    return [CoreDataManager sharedInstance:NSStringFromClass(self)];
}

- (void)modelDidLoad
{
    //模型创建完成后进行初始化
}

// Default
+ (NSFetchedResultsController *)fetchedResultsController
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(self)];
    
    // blank array sort desc is need
    [fetchRequest setSortDescriptors:@[]];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[self sharedManager] mainObjectContext] sectionNameKeyPath:nil cacheName:NSStringFromClass(self)];
    return fetchedResultsController;
}

@end
